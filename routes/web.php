<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\aboutController;
use App\Http\Controllers\skillsController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\mahasiswaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [homeController::class, 'index']);

Route::get('/about', [aboutController::class, 'index']);

Route::get('/skills', [skillsController::class, 'index']);

Route::get('/contact', [contactController::class, 'index']);

Route::get('/mahasiswa', [mahasiswaController::class, 'index']);

Route::get('/mahasiswa/tambah',[mahasiswaController::class, 'tambah']);

Route::post('/mahasiswa/store',[mahasiswaController::class, 'store']);

Route::get('/mahasiswa/edit/{id}',[mahasiswaController::class, 'edit']);

Route::post('/mahasiswa/update',[mahasiswaController::class, 'update']);

Route::get('/mahasiswa/hapus/{id}',[mahasiswaController::class, 'hapus']);
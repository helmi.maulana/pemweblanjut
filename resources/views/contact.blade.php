@extends('layout.main')

@section('judul')
<strong>Contact</strong>
@endsection

@section('isi')
    <center>Apabila ada pertanyaan yang ingin ditanyakan kepada saya, silahkan isikan pada kolom di bawah.</center>
@endsection
@section('form')

<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="form-group mb-4">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Email Anda">

        </div>
        <div class="form-group mb-4">
        <label for="exampleInputPassword1">Nama</label>
        <input type="name" class="form-control" id="exampleInputNama1" placeholder="Nama Anda">
        </div>

        <div class="form-group mb-4">
            <label for="exampleFormControlTextarea1">Pesan</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Pesan yang ingin disampaikan"></textarea>
          </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
  

  @endsection
@extends('layout.main')

@section('judul')
Tambah Mahasiswa
@endsection

@section('skills')
    <br>

    <form action="/mahasiswa/store" method="post">
    {{ csrf_field() }}
    <a href="/mahasiswa">Batal</a><br>
    Nama <input type="text" name="nama" required="required"><br>
    NIM <input type="text" name="nim" required="required"><br>
    Kelas <input type="text" name="kelas" required="required"><br>
    Prodi <input type="text" name="prodi" required="required"><br>
    Fakultas <input type="text" name="fakultas" required="required"><br>
    <input type="submit" value="Simpan Data">
    </form>
@endsection
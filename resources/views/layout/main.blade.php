<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Helmi | {{ $title }}</title>

    <style>
        body{
            background-image: url('img/bgweb3.png');
            background-repeat: no-repeat;
            background-size: cover;
            font-family: 'Fira Sans', sans-serif;
            
        }
        .putih{
          color: #f3f3f3

        }
        .bulet{
            border-radius: 3%;
        }
        .section-title h1::after {
            content: "";
            position: absolute;
            display: block;
            width: 75px;
            height: 3px;
            background: #34b7a7;
            left: calc(50% - 37px);
        }
        .section-title1 h1::after {
            content: "";
            position: absolute;
            display: block;
            width: 75px;
            height: 3px;
            background: #309e91;
            left: calc(50% - 37px);
        }
        .skills .progress {
            height: 60px;
            display: block;
            background: none;
            border-radius: 0;
        }
          .skills .progress .skill {
            padding: 10px 0;
            margin: 0 0 6px 0;
            text-transform: uppercase;
            display: block;
            font-weight: 600;
            font-family: "Poppins", sans-serif;
            color: #222222;
        }
          .skills .progress .skill .val {
            float: right;
            font-style: normal;
        }
          .skills .progress-bar-wrap {
            background: #f3f3f3;
        }
          .skills .progress-bar {
            width: 1px;
            height: 10px;
            background-color: #34b7a7;
        }
          table { 
            border-collapse:collapse;
            border: 1px solid #999;  
            font-family:Arial, sans-serif;
            font-size:16px;
            width:100%;
            caption-side: top;
        }
          caption, table th {
            font-weight:bold;
            padding:10px;
            color:#fff;
            background-color:#2A72BA;
            text-align: center;
        }
          caption, table td {
            padding:10px;
            border-top:1px black solid;
            border-bottom:1px black solid;
            text-align:center; 
        }   
        .btn-tambah
        {
            font-family: "Raleway", sans-serif;
            font-weight: 600;
            font-size: 15px;
            letter-spacing: 1px;
            display: inline-block;
            border-radius: 10px;
            color: #000;
            background: #E0FFFF;
        }
    </style>
  </head>
  <body>

    @include('partial.navbar')

      <div class="container mt-4" data-aos="fade-up" data-aos-delay="100">
          <div class="section-title"><h1 align="center"> @yield('judul') </h1></div>
            <div> @yield('skills')
              <h3> @yield('sub')</h3>
              <p>@yield('isi')</p>
              <p align="center">@yield('isiabout')</p>
              <div>@yield('daleman')</div>
              <form>@yield('form')</form>
            </div>
      </div>

 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script> <script> AOS.init(); </script>
  </body>
</html>
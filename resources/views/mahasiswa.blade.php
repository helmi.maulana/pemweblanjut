@extends('layout.main')
<body>
    @section('judul')
    Data Mahasiswa
    @endsection

    @section('skills')
    <a href="/mahasiswa/tambah"><button class="btn-tambah">Tambah Mahasiswa</button></a>
    <br>

    <table border="1">
        <tr>
            <th>No</th>
            <th>Nama Mahasiswa</th>
            <th>NIM</th>
            <th>Kelas</th>
            <th>Prodi</th>
            <th>Fakultas</th>
            <th>Menu</th>
        </tr>

        @foreach ($mahasiswa as $mhs)

        <tr>
            <td>{{ $mhs->id }}</td>
            <td>{{ $mhs->nama_mahasiswa }}</td>
            <td>{{ $mhs->nim_mahasiswa }}</td>
            <td>{{ $mhs->kelas_mahasiswa }}</td>
            <td>{{ $mhs->prodi_mahasiswa }}</td>
            <td>{{ $mhs->fakultas_mahasiswa }}</td>
            <td>
                <a href="/mahasiswa/edit/{{ $mhs->id }}"><button>Edit</button></a>
                <a href="/mahasiswa/hapus/{{ $mhs->id }}"><button>Hapus</button></a>
            </td>
        </tr>
    
        @endforeach
    </table>        
    @endsection
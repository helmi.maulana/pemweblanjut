@extends('layout.main')

@section('judul')
<strong>Skills</strong>
@endsection

@section('skills')
<section id="skills" class="skills">
    <div class="container" data-aos="fade-up">
        {{-- <p>
          Berikut adalah skill
        </p> --}}
      </div>
      <br>
      <div class="row skills-content">

        <div class="col-lg-6">

          <div class="progress">
            <span class="skill">Bahasa Indonesia <i class="val">95%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width:95%"></div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Bahasa Inggris <i class="val">80%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width: 80%"></div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Microsoft Word <i class="val">75%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width: 75%"></div>
            </div>
          </div>

        </div>

        <div class="col-lg-6">

          <div class="progress">
            <span class="skill">Adobe Premiere Pro<i class="val">75%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width: 75%"></div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Adobe Photoshop<i class="val">65%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width: 65%"></div>
            </div>
          </div>

          <div class="progress">
            <span class="skill">Adobe After Effect<i class="val">65%</i></span>
            <div class="progress-bar-wrap">
              <div class="progress-bar" style="width: 65%"></div>
            </div>
          </div>

        </div>

      </div>

    </div>
  </section>
@endsection
@extends('layout.main')

@section('judul')
<strong>About</strong>    

@endsection

@section('isiabout')
    {{-- Nama lengkap saya adalah Helmi Maulana Hadiwinata, teman-teman saya biasa memanggil saya Helmi atau Hel saja. 
    saya lahir di negara tanggal 3 juni 2001. <br/>
    Saya tinggal di Baktiseraga, Singaraja, Buleleng, Bali. Saya anak pertama dari dua bersaudara.--}}
@endsection


@section('daleman')

            {{-- <h3><center><u>Helmi Maulana Hadiwinata</u></center></h3> --}}
            <img src="img/sy1.jpg" class="bulet" style="float: left; max-width:13%; max-height:13%"/>
                {{-- <p style="margin-left: 200px">
                    Pepatah China Pernah Mengatakan
                    <strong>"Super Idol de xiaorong
                    dou mei ni de tian
                    ba yue zhengwu de yangguang
                    dou mei ni yaoyan
                    re’ai 105 °C de ni
                    di di qingchun de zhengliushui🔥🔥🔥"</strong>
                </p> --}} <br>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-rounded-right"></i> <strong>Tanggal Lahir:</strong> 3 Juni 2001</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Tempat Lahir:</strong> Negara</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Whatsapp:</strong> 082147143196</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Jenis Kelamin:</strong> Laki-Laki</li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-rounded-right"></i> <strong>Umur:</strong> 20</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Agama:</strong> Islam</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Email:</strong> helmiitu1@gmail.com</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Pekerjaan:</strong> Pelajar/Mahasiswa</li>
                </ul>
              </div>
            </div>
            <p style="margin-left: 200px">
              Nama lengkap saya adalah Helmi Maulana Hadiwinata, teman-teman saya biasa memanggil saya Helmi atau Hel saja. 
              saya lahir di negara tanggal 3 juni 2001.
              Saya tinggal di Baktiseraga, Singaraja, Buleleng, Bali. Saya anak pertama dari dua bersaudara.
              Hobi saya adalah berenang dan bermain video game, salah satu video game favorit saya adalah Dota 2.
            </p>
            <br><br>
          
          <div class="section-title1"> 
            <center><h1><strong>Pendidikan</strong></h1></center> <br>
            <div class="row">
              <div class="col-6">
                <strong><h3>Taman Kanak-Kanak (TK)</h3>
                <p>Saya berasal dari TK RA Ath-Thooriq yang berada di Jalan Merak no 2, Singaraja, Buleleng, Bali</p>
                <h3>Sekolah Dasar (SD)</h3>
                <p>Saya berasal dari SD MIT Mardlatillah yang berada di Jalan Jalak Putih 1 no 1, Singaraja, Buleleng, Bali</p></strong>
              </div>
              <div class="col-6">
                <strong><h3>Sekolah Menengah Pertama (SMP)</h3>
                <p>Saya berasal dari SMP MTsT Mardlatillah yang berada di Jalan Jalak Putih 1 no 1, Singaraja, Buleleng, Bali</p>
                <h3>Sekolah Menengah Atas</h3>
                <p>Saya berasal dari SMAs Laboratorium atau yang biasa dikenal SMA Lab Undiksha Singaraja yang berada di Jalan Jatayu no 10, Singaraja, Buleleng, Bali</p></strong>
              </div>
            </div>
          </div>

@endsection
<!doctype html>
<html lang="en">
  <head>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>Helmi | {{ $title }}</title>
   
    <style>
        body
        {
            background-image: url('img/bghome.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            font-family: 'Fira Sans', sans-serif;
        }

        h1
        {
            color:white
        }

        h3
        {
            color:white
        }

        .btn-about
        {
            font-family: "Raleway", sans-serif;
            text-transform: uppercase;
            font-weight: 600;
            font-size: 15px;
            letter-spacing: 1px;
            font-weight:700;
            display: inline-block;
            padding: 12px 40px;
            border-radius: 50px;
            transition: 0.5s;
            margin-top: 500px;
            color: #000;
            background: #D9B48FFF;
        }
        .tengah
        {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        
    </style>

  </head>
  <body>

    @include('partial.navbar')

        <div class="container mt-5" data-aos='zoom-out' data-aos-duration='500'>
                <h1><center>Helmi Maulana Hadiwinata</center></h1>
                <h3><center>Mahasiswa Aktif Universitas Pendidikan Ganesha Angkatan 2019</center></h3>
                
                    <div class="tengah">
                        {{-- <br><br><br> --}}
                        <center><a href="/about"><button class="btn-about">About Me</button></a></center>
                    </div>
        </div>
 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script> <script> AOS.init(); </script>
  </body>
</html>
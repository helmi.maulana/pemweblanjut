@extends('layout.main')

@section('judul')
Edit Data Mahasiswa
@endsection

@section('skills')
    <br>

    @foreach($mahasiswa as $mhs)
	<form action="/mahasiswa/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $mhs->id }}"> <br/>
		Nama <input type="text" name="nama" required="required" value="{{ $mhs->nama_mahasiswa }}"> <br/>
		NIM <input type="text" required="required" name="nim" value="{{ $mhs->nim_mahasiswa }}"> <br/>
		Kelas <input type="text" required="required" name="kelas" value="{{ $mhs->kelas_mahasiswa }}"> <br/>
		Prodi <input type="text" required="required" name="prodi" value="{{ $mhs->prodi_mahasiswa }}"> <br/>
        Fakultas<input type="text" required="required" name="fakultas" value="{{ $mhs->fakultas_mahasiswa }}"> <br>
		<a href="/mahasiswa"><button>Batal</button></a><input type="submit" value="Simpan Data">
	</form>
	@endforeach
@endsection
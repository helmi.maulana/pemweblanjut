<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class mahasiswaController extends Controller
{
    public function index()
     {
        $mahasiswa = DB::table('mahasiswa')->get();

        return view('mahasiswa',["title"=>'Mahasiswa'],['mahasiswa'=>$mahasiswa]);
        
     }

    public function tambah()
    {
        return view('tambah', ["title"=>'Tambah']);
    }

    public function store(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'=>$request->nama,
            'nim_mahasiswa'=>$request->nim,
            'kelas_mahasiswa'=>$request->kelas,
            'prodi_mahasiswa'=>$request->prodi,
            'fakultas_mahasiswa'=>$request->fakultas
        ]);

        return redirect('/mahasiswa');
    }

    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get();
        return view('edit', ["title"=>'Edit'],['mahasiswa'=>$mahasiswa]);
    }

    public function update(Request $request)
    {
	    DB::table('mahasiswa')->where('id',$request->id)->update([
		'nama_mahasiswa' => $request->nama,
		'nim_mahasiswa' => $request->nim,
		'kelas_mahasiswa' => $request->kelas,
		'prodi_mahasiswa' => $request->prodi,
        'fakultas_mahasiswa' => $request->fakultas
	]);

	    return redirect('/mahasiswa');
    }

    public function hapus($id)
    {
	DB::table('mahasiswa')->where('id',$id)->delete();
		
	return redirect('/mahasiswa');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class skillsController extends Controller
{
    public function index()
    {
        return view('skills', [
            "title" => 'Skills'
        ]);
    }
}
